/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "s32k144_gpio.h"

void s32k144_gpio_pin_configure(uint32_t pin, uint32_t mode) {
    uint32_t dir, mux, pull, drv;

    dir = (mode & GPIO_DIR_MASK);
    mux = (mode & GPIO_MUX_MASK);
    pull = (mode & GPIO_PULL_MASK);
    drv = (mode & GPIO_DRIVE_MASK);

    s32k144_gpio_pin_dir(pin, dir);

    s32k144_gpio_pin_mux(pin, mux);

    s32k144_gpio_pin_pull(pin, pull);

    s32k144_gpio_pin_drive(pin, drv);
}

void s32k144_gpio_pin_dir(uint32_t pin, uint32_t dir) {
    GPIO_Type *gpio;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    gpio = (GPIO_Type*)(PTA_BASE + (PTB_BASE - PTA_BASE) * grp);
    if (dir == GPIO_DIR_ANALOG) {
        gpio->PIDR |= (0x1 << idx);
        dir = GPIO_DIR_INPUT;
    }
    else {
        gpio->PIDR &= ~(0x1 << idx);
    }
    dir >>= GPIO_DIR_SHIFT;
    gpio->PDDR &= ~GPIO_PDDR_PDD_MASK;
    gpio->PDDR |= (dir << idx);
}

void s32k144_gpio_pin_mux(uint32_t pin, uint32_t mux) {
    PORT_Type *port;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    port = (PORT_Type*)(PORTA_BASE + (PORTB_BASE - PORTA_BASE) * grp);
    mux >>= GPIO_MUX_SHIFT;
    port->PCR[idx] &= ~PORT_PCR_MUX_MASK;
    port->PCR[idx] |= (mux << PORT_PCR_MUX_SHIFT);
}

void s32k144_gpio_pin_pull(uint32_t pin, uint32_t pull) {
    PORT_Type *port;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    port = (PORT_Type*)(PORTA_BASE + (PORTB_BASE - PORTA_BASE) * grp);
    pull >>= GPIO_MUX_SHIFT;
    port->PCR[idx] &= ~(PORT_PCR_PE_MASK | PORT_PCR_PS_MASK);
    port->PCR[idx] |= (pull << GPIO_PULL_SHIFT);
}

void s32k144_gpio_pin_drive(uint32_t pin, uint32_t drive) {
    PORT_Type *port;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    port = (PORT_Type*)(PORTA_BASE + (PORTB_BASE - PORTA_BASE) * grp);
    drive >>= GPIO_DRIVE_SHIFT;
    port->PCR[idx] &= ~PORT_PCR_DSE_MASK;
    port->PCR[idx] |= (drive << GPIO_DRIVE_SHIFT);
}

