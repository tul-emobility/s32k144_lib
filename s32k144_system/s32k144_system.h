/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef S32K144_SYSTEM_H_
#define S32K144_SYSTEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "device_registers.h"
#include "system_S32K144.h"

typedef enum {
    S32K144_PERIPHERAL_FTFC,
    S32K144_PERIPHERAL_DMAMUX,
    S32K144_PERIPHERAL_FLEXCAN0,
    S32K144_PERIPHERAL_FLEXCAN1,
    S32K144_PERIPHERAL_FLEXCAN2,
    S32K144_PERIPHERAL_LPSPI0,
    S32K144_PERIPHERAL_LPSPI1,
    S32K144_PERIPHERAL_LPSPI2,
    S32K144_PERIPHERAL_PDB0,
    S32K144_PERIPHERAL_PDB1,
    S32K144_PERIPHERAL_CRC,
    S32K144_PERIPHERAL_LPIT,
    S32K144_PERIPHERAL_FTM0,
    S32K144_PERIPHERAL_FTM1,
    S32K144_PERIPHERAL_FTM2,
    S32K144_PERIPHERAL_FTM3,
    S32K144_PERIPHERAL_ADC0,
    S32K144_PERIPHERAL_ADC1,
    S32K144_PERIPHERAL_RTC,
    S32K144_PERIPHERAL_LPTMR0,
    S32K144_PERIPHERAL_PORTA,
    S32K144_PERIPHERAL_PORTB,
    S32K144_PERIPHERAL_PORTC,
    S32K144_PERIPHERAL_PORTD,
    S32K144_PERIPHERAL_PORTE,
    S32K144_PERIPHERAL_FLEXIO,
    S32K144_PERIPHERAL_EWM,
    S32K144_PERIPHERAL_LPI2C0,
    S32K144_PERIPHERAL_LPUART0,
    S32K144_PERIPHERAL_LPUART1,
    S32K144_PERIPHERAL_LPUART2,
    S32K144_PERIPHERAL_CMP0,
    S32K144_PERIPHERAL_COUNT
} s32k144_periph_t;

void s32k144_system_periph_enable(s32k144_periph_t periph);

void s32k144_system_periph_disable(s32k144_periph_t periph);

void s32k144_system_initialize(void);

void s32k144_system_firc_enable(void);

void s32k144_system_firc_disable(void);

void s32k144_system_sirc_enable(void);

void s32k144_system_sirc_disable(void);

void s32k144_system_sosc_enable(void);

void s32k144_system_sosc_disable(void);

void s32k144_system_spll_enable(void);

void s32k144_system_spll_disable(void);

void s32k144_system_lpo_enable(void);

#ifdef __cplusplus
}
#endif

#endif /* S32K144_SYSTEM_H_ */
