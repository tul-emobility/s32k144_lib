/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef S32K144_GPIO_H_
#define S32K144_GPIO_H_

#include "device_registers.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  [PIN4][PIN3][PIN2][PIN1][PIN0][-][PORT2][PORT1][PORT0]
 */

#define GPIO_PORT_MASK              0x7
#define GPIO_PORT_SHIFT             0

#define GPIO_PORT_A                 0
#define GPIO_PORT_B                 1
#define GPIO_PORT_C                 2
#define GPIO_PORT_D                 3
#define GPIO_PORT_E                 4

#define GPIO_PIN_MASK               0x1f0
#define GPIO_PIN_SHIFT              4

#define GPIO_PIN_0                  0x000
#define GPIO_PIN_1                  0x010
#define GPIO_PIN_2                  0x020
#define GPIO_PIN_3                  0x030
#define GPIO_PIN_4                  0x040
#define GPIO_PIN_5                  0x050
#define GPIO_PIN_6                  0x060
#define GPIO_PIN_7                  0x070
#define GPIO_PIN_8                  0x080
#define GPIO_PIN_9                  0x090
#define GPIO_PIN_10                 0x0a0
#define GPIO_PIN_11                 0x0b0
#define GPIO_PIN_12                 0x0c0
#define GPIO_PIN_13                 0x0d0
#define GPIO_PIN_14                 0x0e0
#define GPIO_PIN_15                 0x0f0
#define GPIO_PIN_16                 0x100
#define GPIO_PIN_17                 0x110

#define GPIO_PIN(_port, _pin)       (uint32_t)(((_pin << GPIO_PIN_SHIFT) & GPIO_PIN_MASK) | (_port & GPIO_PORT_MASK))

/*
 * [DRIVE0][-][-][PULL1][PULL0][MUX3][MUX2][MUX1][MUX0][-][-][DIR1][DIR0]
 */

#define GPIO_DIR_MASK               0x3
#define GPIO_DIR_SHIFT              0

#define GPIO_DIR_INPUT              0x0
#define GPIO_DIR_OUTPUT             0x1
#define GPIO_DIR_ANALOG             0x2

#define GPIO_MUX_MASK               0xf0
#define GPIO_MUX_SHIFT              4

#define GPIO_MUX_NONE               0x00
#define GPIO_MUX_IO                 0x10
#define GPIO_MUX_1                  0x20
#define GPIO_MUX_2                  0x30
#define GPIO_MUX_3                  0x40
#define GPIO_MUX_4                  0x50
#define GPIO_MUX_5                  0x60
#define GPIO_MUX_6                  0x70
#define GPIO_MUX_7                  0x80

#define GPIO_PULL_MASK              0x300
#define GPIO_PULL_SHIFT             8

#define GPIO_PULL_NONE              0x000
#define GPIO_PULL_DOWN              0x200
#define GPIO_PULL_UP                0x300

#define GPIO_DRIVE_MASK             0x10000
#define GPIO_DRIVE_SHIFT            16

#define GPIO_DRIVE_LOW              0x00000
#define GPIO_DRIVE_HIGH             0x10000

void s32k144_gpio_pin_configure(uint32_t pin, uint32_t mode);

void s32k144_gpio_pin_dir(uint32_t pin, uint32_t dir);

void s32k144_gpio_pin_mux(uint32_t pin, uint32_t mux);

void s32k144_gpio_pin_pull(uint32_t pin, uint32_t pull);

void s32k144_gpio_pin_drive(uint32_t pin, uint32_t drive);

static inline void s32k144_gpio_pin_set(uint32_t pin) {
    GPIO_Type *gpio;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    gpio = (GPIO_Type *)(PTA_BASE + (PTB_BASE - PTA_BASE) * grp);
    gpio->PSOR |= (0x1 << idx);
}

static inline void s32k144_gpio_pin_reset(uint32_t pin) {
    GPIO_Type *gpio;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    gpio = (GPIO_Type *)(PTA_BASE + (PTB_BASE - PTA_BASE) * grp);
    gpio->PCOR |= (0x1 << idx);
}

static inline void s32k144_gpio_pin_toggle(uint32_t pin) {
    GPIO_Type *gpio;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    gpio = (GPIO_Type *)(PTA_BASE + (PTB_BASE - PTA_BASE) * grp);
    gpio->PTOR |= (0x1 << idx);
}

static inline uint32_t s32k144_gpio_pin_read(uint32_t pin) {
    GPIO_Type *gpio;
    uint32_t grp, idx;

    grp = (pin & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    idx = (pin & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;

    gpio = (GPIO_Type *)(PTA_BASE + (PTB_BASE - PTA_BASE) * grp);
    return gpio->PDIR & (0x1 << idx);
}

void s32k144_gpio_pin_notify(uint32_t pin, uint32_t mode);

#ifdef __cplusplus
}
#endif

#endif /* S32K144_GPIO_H_ */
