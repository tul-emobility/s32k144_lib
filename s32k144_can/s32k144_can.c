/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "s32k144_can.h"

#include "device_registers.h"

typedef struct {
    flexcan_state_t state;
    bool initialized;
    s32k144_can_callback_t callback;
    void *context;
} s32k144_can_handle_t;

typedef struct {
    s32k144_can_handle_t handles[S32K144_CAN_COUNT];
} s32k144_can_driver_t;

static s32k144_can_driver_t _s32k144_can_driver;

static void _s32k144_flexcan_callback(uint8_t instance, flexcan_event_type_t eventType, uint32_t buffIdx,
        flexcan_state_t *flexcanState) {
    s32k144_can_callback_t callback = _s32k144_can_driver.handles[instance].callback;
    void* context = _s32k144_can_driver.handles[instance].context;
    switch (eventType) {
        case FLEXCAN_EVENT_TX_COMPLETE:
            callback(instance, buffIdx, S32K144_CAN_TX_COMPLETE, context);
            break;
        case FLEXCAN_EVENT_RX_COMPLETE:
            callback(instance, buffIdx, S32K144_CAN_RX_COMPLETE, context);
            break;
        default:
            break;
    }
}

status_t s32k144_can_init(const s32k144_can_inst_t inst, const s32k144_can_cfg_t *cfg) {
    status_t status = STATUS_ERROR;
    if (_s32k144_can_driver.handles[inst].initialized)
    {
        return status;
    }
    flexcan_state_t *state = &_s32k144_can_driver.handles[inst].state;
    flexcan_user_config_t config = { 0 };
    config.max_num_mb = cfg->mailbox_count;
    config.flexcanMode = (flexcan_operation_modes_t)cfg->mode;
    config.pe_clock = (flexcan_clk_source_t)cfg->clk;
    config.bitrate.propSeg = cfg->timing.prs;
    config.bitrate.phaseSeg1 = cfg->timing.phs1;
    config.bitrate.phaseSeg2 = cfg->timing.phs2;
    config.bitrate.preDivider = cfg->timing.div;
    config.bitrate.rJumpwidth = cfg->timing.sjw;
    config.is_rx_fifo_needed = false;
    config.num_id_filters = FLEXCAN_RX_FIFO_ID_FILTERS_8;
    config.transfer_type = FLEXCAN_RXFIFO_USING_INTERRUPTS;
    status = FLEXCAN_DRV_Init((uint8_t)inst, state, &config);
    if (status == STATUS_SUCCESS) {
        _s32k144_can_driver.handles[inst].initialized = true;
    }
    return status;
}

status_t s32k144_can_deinit(s32k144_can_inst_t inst) {
    if (!_s32k144_can_driver.handles[inst].initialized)
    {
        return STATUS_SUCCESS;
    }
    return FLEXCAN_DRV_Deinit((uint8_t)inst);
}

status_t s32k144_can_set_timing(s32k144_can_inst_t inst, s32k144_can_timing_t *timing) {
    status_t status = STATUS_ERROR;
    if (!_s32k144_can_driver.handles[inst].initialized)
    {
        return status;
    }
    status = STATUS_SUCCESS;
    flexcan_time_segment_t bitrate;
    bitrate.propSeg = timing->prs;
    bitrate.phaseSeg1 = timing->phs1;
    bitrate.phaseSeg2 = timing->phs2;
    bitrate.preDivider = timing->div;
    bitrate.rJumpwidth = timing->sjw;
    FLEXCAN_DRV_SetBitrate(inst, &bitrate);
    return status;
}

status_t s32k144_can_tx_mailbox_cfg(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_id_type_t idt) {
    flexcan_data_info_t datainfo = { 0 };
    datainfo.msg_id_type = (flexcan_msgbuff_id_type_t)idt;
    datainfo.data_length = (uint32_t)8;
    datainfo.is_remote = false;
    return FLEXCAN_DRV_ConfigTxMb((uint8_t)inst, (uint8_t)idx, &datainfo, 0);
}

status_t s32k144_can_rx_mailbox_cfg(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_id_type_t idt, uint32_t id) {
    flexcan_data_info_t datainfo = { 0 };
    datainfo.msg_id_type = (flexcan_msgbuff_id_type_t)idt;
    datainfo.data_length = (uint32_t)8;
    datainfo.is_remote = false;
    return FLEXCAN_DRV_ConfigRxMb((uint8_t)inst, idx, &datainfo, id);
}

status_t s32k144_can_transmit(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_msg_t *message) {
    s32k144_can_id_type_t type = CAN_MSG_ID_STD;
    if (message->id > 0x7ff) {
        type = CAN_MSG_ID_EXT;
    }
    flexcan_data_info_t datainfo = { 0 };
    datainfo.msg_id_type = (flexcan_msgbuff_id_type_t)type;
    datainfo.data_length = (uint32_t)message->dlc;
    datainfo.is_remote = false;
    return FLEXCAN_DRV_Send((uint8_t)inst, idx, &datainfo, message->id, message->data);
}

status_t s32k144_can_receive(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_msg_t *message) {
    return FLEXCAN_DRV_Receive((uint8_t)inst, idx, (flexcan_msgbuff_t*)&message);
}

status_t s32k144_can_get_mailbox_status(s32k144_can_inst_t inst, uint32_t idx) {
    return FLEXCAN_DRV_GetTransferStatus((uint8_t)inst, (uint8_t)idx);
}

status_t s32k144_can_notify(s32k144_can_inst_t inst, s32k144_can_callback_t callback, void * context) {
    _s32k144_can_driver.handles[inst].callback = callback;
    _s32k144_can_driver.handles[inst].context = context;
    FLEXCAN_DRV_InstallEventCallback((uint8_t)inst, _s32k144_flexcan_callback, context);
    return STATUS_SUCCESS;
}

