/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef S32K144_CAN_H_
#define S32K144_CAN_H_

#include "flexcan_driver.h"
#include "status.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    S32K144_CAN0,
    S32K144_CAN1,
    S32K144_CAN2,
    S32K144_CAN_COUNT
} s32k144_can_inst_t;

typedef enum {
    CAN_MODE_NORMAL,
    CAN_MODE_LOOPBACK,
    CAN_MODE_DISABLE
} s32k144_can_mode_t;

typedef struct {
    uint32_t prs;
    uint32_t phs1;
    uint32_t phs2;
    uint32_t div;
    uint32_t sjw;
} s32k144_can_timing_t;

typedef enum {
    CAN_MSG_ID_STD,
    CAN_MSG_ID_EXT
} s32k144_can_id_type_t;

typedef enum {
    CAN_CLKSRC_OSC,
    CAN_CLKSRC_PERIPH
} s32k144_can_clk_t;

typedef struct {
    uint32_t cs;
    uint32_t id;
    uint8_t data[8];
    uint8_t dlc;
} s32k144_can_msg_t;

typedef struct {
    uint32_t mailbox_count;
    s32k144_can_mode_t mode;
    s32k144_can_clk_t clk;
    s32k144_can_timing_t timing;
} s32k144_can_cfg_t;

typedef enum {
    S32K144_CAN_RX_COMPLETE,
    S32K144_CAN_TX_COMPLETE
} s32k144_can_event_t;

typedef void (*s32k144_can_callback_t)(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_event_t event, void *context);

status_t s32k144_can_init(s32k144_can_inst_t inst, const s32k144_can_cfg_t *cfg);

status_t s32k144_can_deinit(s32k144_can_inst_t inst);

status_t s32k144_can_set_timing(s32k144_can_inst_t inst, s32k144_can_timing_t *timing);

status_t s32k144_can_tx_mailbox_cfg(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_id_type_t idt);

status_t s32k144_can_rx_mailbox_cfg(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_id_type_t idt, uint32_t id);

status_t s32k144_can_transmit(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_msg_t *message);

status_t s32k144_can_receive(s32k144_can_inst_t inst, uint32_t idx, s32k144_can_msg_t *message);

status_t s32k144_can_get_mailbox_status(s32k144_can_inst_t inst, uint32_t idx);

status_t s32k144_can_notify(s32k144_can_inst_t inst, s32k144_can_callback_t callback, void * context);

#ifdef __cplusplus
}
#endif

#endif /* S32K144_CAN_H_ */
