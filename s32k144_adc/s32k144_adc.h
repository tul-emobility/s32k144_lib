/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef S32K144_ADC_H_
#define S32K144_ADC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "adc_driver.h"
#include "status.h"

typedef enum {
    S32K144_ADC_0,
    S32K144_ADC_1,
    S32K144_ADC_COUNT
} s32k144_adc_inst_t;

typedef adc_inputchannel_t s32k144_adc_channel_t;

typedef void (*s32k144_adc_callback_t)(uint32_t group, uint32_t tail, void * context);

typedef struct {
    const adc_inputchannel_t *channels;
    uint16_t *results;
    uint8_t channel_count;
    uint8_t result_count;
    bool continuous;
    s32k144_adc_callback_t callback;
    void * context;
} s32k144_adc_group_cfg_t;

typedef struct {
    const s32k144_adc_group_cfg_t * groups;
    uint8_t group_count;
    uint8_t sample_ticks;
} s32k144_adc_cfg_t;

status_t s32k144_adc_init(s32k144_adc_inst_t inst, s32k144_adc_cfg_t *cfg);

status_t s32k144_adc_deinit(s32k144_adc_inst_t inst);

status_t s32k144_adc_start_group_conversion(s32k144_adc_inst_t inst, uint32_t group, uin32_t timeout);

status_t s32k144_adc_stop_group_conversion(s32k144_adc_inst_t inst, uint32_t group, uint32_t timeout);

status_t s32k144_adc_notify(s32k144_adc_inst_t inst, s32k144_adc_callback_t callback, void *context);

#ifdef __cplusplus
}
#endif

#endif /* S32K144_ADC_H_ */
