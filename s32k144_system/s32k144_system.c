/* MIT License
 *
 * Copyright (c) 2020 Zoltan Dolensky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "s32k144_system.h"

static uint32_t const _s32k144_periph_pcc_idx[S32K144_PERIPHERAL_COUNT] = {
PCC_FTFC_INDEX, /* S32K144_PERIPHERAL_FTFC */
    PCC_DMAMUX_INDEX, /*S32K144_PERIPHERAL_DMAMUX */
    PCC_FlexCAN0_INDEX, /* S32K144_PERIPHERAL_FLEXCAN0 */
    PCC_FlexCAN1_INDEX, /* S32K144_PERIPHERAL_FLEXCAN1 */
    PCC_FlexCAN2_INDEX, /* S32K144_PERIPHERAL_FLEXCAN2 */
    PCC_LPSPI0_INDEX, /* S32K144_PERIPHERAL_LPSPI0 */
    PCC_LPSPI1_INDEX, /* S32K144_PERIPHERAL_LPSPI1 */
    PCC_LPSPI2_INDEX, /* S32K144_PERIPHERAL_LPSPI2 */
    PCC_PDB0_INDEX, /* S32K144_PERIPHERAL_PDB0 */
    PCC_PDB1_INDEX, /* S32K144_PERIPHERAL_PDB1 */
    PCC_CRC_INDEX, /* S32K144_PERIPHERAL_CRC */
    PCC_LPIT_INDEX, /* S32K144_PERIPHERAL_LPIT */
    PCC_FTM0_INDEX, /* S32K144_PERIPHERAL_FTM0 */
    PCC_FTM1_INDEX, /* S32K144_PERIPHERAL_FTM1 */
    PCC_FTM2_INDEX, /* S32K144_PERIPHERAL_FTM2 */
    PCC_FTM3_INDEX, /* S32K144_PERIPHERAL_FTM3 */
    PCC_ADC0_INDEX, /* S32K144_PERIPHERAL_ADC0 */
    PCC_ADC1_INDEX, /* S32K144_PERIPHERAL_ADC1 */
    PCC_RTC_INDEX, /* S32K144_PERIPHERAL_RTC */
    PCC_LPTMR0_INDEX, /* S32K144_PERIPHERAL_LPTMR0 */
    PCC_PORTA_INDEX, /* S32K144_PERIPHERAL_PORTA */
    PCC_PORTB_INDEX, /* S32K144_PERIPHERAL_PORTB */
    PCC_PORTC_INDEX, /* S32K144_PERIPHERAL_PORTC */
    PCC_PORTD_INDEX, /* S32K144_PERIPHERAL_PORTD */
    PCC_PORTE_INDEX, /* S32K144_PERIPHERAL_PORTE */
    PCC_FlexIO_INDEX, /* S32K144_PERIPHERAL_FLEXIO */
    PCC_EWM_INDEX, /* S32K144_PERIPHERAL_EWM */
    PCC_LPI2C0_INDEX, /* S32K144_PERIPHERAL_LPI2C0 */
    PCC_LPUART0_INDEX, /* S32K144_PERIPHERAL_LPUART0 */
    PCC_LPUART1_INDEX, /* S32K144_PERIPHERAL_LPUART1 */
    PCC_LPUART2_INDEX, /* S32K144_PERIPHERAL_LPUART2 */
    PCC_CMP0_INDEX /* S32K144_PERIPHERAL_CMP0 */
};

void s32k144_system_periph_enable(s32k144_periph_t periph) {
    if (periph > S32K144_PERIPHERAL_COUNT) {
        return;
    }
    PCC->PCCn[_s32k144_periph_pcc_idx[periph]] |= PCC_PCCn_CGC_MASK;
}

void s32k144_system_periph_disable(s32k144_periph_t periph) {
    if (periph > S32K144_PERIPHERAL_COUNT) {
        return;
    }
    PCC->PCCn[_s32k144_periph_pcc_idx[periph]] &= ~PCC_PCCn_CGC_MASK;
}

void s32k144_system_initialize(void) {
    DISABLE_INTERRUPTS()
    ;
    uint32_t val = 0;
    /* Configure SIRC functional clock outputs */
    val |= (0x1 << SCG_SIRCDIV_SIRCDIV1_SHIFT);
    val |= (0x1 << SCG_SIRCDIV_SIRCDIV2_SHIFT);
    SCG->SIRCDIV = val;
    /* Switch to SIRC temporarily */
    s32k144_system_sirc_enable();
    val = SCG->RCCR;
    val &= ~SCG_RCCR_SCS_MASK;
    val |= (0x2 << SCG_RCCR_SCS_SHIFT);
    SCG->RCCR = val;
    SystemCoreClock = 8000000;
    /* Configure FIRC functional clock outputs */
    val = 0;
    s32k144_system_firc_disable();
    val |= (0x1 << SCG_FIRCDIV_FIRCDIV1_SHIFT);
    val |= (0x1 << SCG_FIRCDIV_FIRCDIV2_SHIFT);
    SCG->FIRCDIV = val;
    s32k144_system_firc_enable();
    /* Configure RUN mode clocks */
    val = 0;
    val |= (0x3 << SCG_RCCR_SCS_SHIFT);
    val |= (0x1 << SCG_RCCR_DIVSLOW_SHIFT);
    SCG->RCCR = val;
    SystemCoreClock = 48000000;
    /* Configure VLPR mode clocks */
    val = 0;
    val |= (0x2 << SCG_VCCR_SCS_SHIFT);
    val |= (0x1 << SCG_VCCR_DIVCORE_SHIFT);
    val |= (0x1 << SCG_VCCR_DIVBUS_SHIFT);
    val |= (0x7 << SCG_VCCR_DIVSLOW_SHIFT);
    SCG->VCCR = val;
    /* Configure HSRUN mode clocks */
    val = 0;
    val |= (0x3 << SCG_HCCR_SCS_SHIFT);
    val |= (0x1 << SCG_HCCR_DIVSLOW_SHIFT);
    SCG->HCCR = val;
#if 0 /* Unused until new hardware revision with fixed crystal */
    s32k144_system_sosc_enable();
    s32k144_system_spll_enable();
#endif
    ENABLE_INTERRUPTS()
    ;
}

void s32k144_system_firc_enable(void) {
    SCG->FIRCCSR = (uint32_t)SCG_FIRCCSR_FIRCEN_MASK;
    while (!(SCG->FIRCCSR & SCG_FIRCCSR_FIRCVLD_MASK)) {
    }
}

void s32k144_system_firc_disable(void) {
    SCG->FIRCCSR = (uint32_t)0x00;
}

void s32k144_system_sosc_enable(void) {
    SCG->SOSCCFG = (SCG_SOSCCFG_RANGE_MASK | SCG_SOSCCFG_HGO_MASK | SCG_SOSCCFG_EREFS_MASK);
    SCG->SOSCCSR = (uint32_t)(SCG_SOSCCSR_SOSCEN_MASK);
    while (!(SCG->SOSCCSR & SCG_SOSCCSR_SOSCVLD_MASK)) {
    }
}

void s32k144_system_sosc_disable(void) {
    SCG->SOSCCSR = (uint32_t)0x0;
}

void s32k144_system_sirc_enable(void) {
    SCG->SIRCCFG = (uint32_t)SCG_SIRCCFG_RANGE_MASK;
    SCG->SIRCCSR = (uint32_t)(SCG_SIRCCSR_SIRCEN_MASK | SCG_SIRCCSR_SIRCSTEN_MASK | SCG_SIRCCSR_SIRCLPEN_MASK);
    while (!(SCG->SIRCCSR & SCG_SIRCCSR_SIRCVLD_MASK)) {
    }
}

void s32k144_system_sirc_disable(void) {
    SCG->SIRCCSR = (uint32_t)0x0;
    while (SCG->SIRCCSR & SCG_SIRCCSR_SIRCVLD_MASK) {
    }
}

void s32k144_system_spll_enable(void) {
    SCG->SPLLCSR = (uint32_t)SCG_SPLLCSR_SPLLEN_MASK;
    while (!(SCG->SPLLCSR & SCG_SPLLCSR_SPLLVLD_MASK)) {
    }
}

void s32k144_system_spll_disable(void) {
    SCG->SPLLCSR = (uint32_t)0x0;
}

void s32k144_system_lpo_enable(void) {
    uint32_t val = 0;
    /* enable 32 khz clock (128 khz / 4 ) */
    val |= SIM_LPOCLKS_LPO32KCLKEN_MASK;
    /* select 32 khz clock as lpo clk source */
    val |= (0x2 << SIM_LPOCLKS_LPOCLKSEL_SHIFT);
    /* select 32 khz lpo as rtc clk source */
    val |= (0x1 << SIM_LPOCLKS_RTCCLKSEL_SHIFT);
    SIM->LPOCLKS = val;
}

